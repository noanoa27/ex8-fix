import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  text;
  id;
  key;
  showTheButton=false;
  showEditField = false;
  tempText;
 // checkboxFlag: boolean;
 status: boolean;

 
 @Input() data:any;
 @Output() myButtonClicked = new EventEmitter<any>();


 
  delete() {
    this.TodosService.deleteTodo(this.key);
  }
  checkChange()//checkbox
 {
 // this.TodosService.updateDone(this.key,this.text,this.checkboxFlag);
 this.TodosService.updateStatus(this.key,this.text,this.status);
 }
 
  showEdit() {
    this.showEditField = true;
     this.tempText = this.text;
  }
  
  cancel() {
    this.showEditField = false;
    this.text = this.tempText;
  }
 
  save() {
  //  this.TodosService.update(this.key, this.text);
    this.TodosService.updateTodo(this.key,this.text, this.status);
    this.showEditField = false;
  }
  
  constructor(private TodosService:TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.id = this.data.id;
    this.key = this.data.$key;
    //this.checkboxFlag = this.data.done;
    this.status = this.data.status;
  }

}
