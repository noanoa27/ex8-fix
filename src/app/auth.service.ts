import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  signup(email:string,password:string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);
    }
    
    updateProfile(user,name:string){
      user.updateProfile({displayName:name,photoURL:''});
    }

    login(email: string, password: string) {
      return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
    }
   
    logOut() {
      return this.fireBaseAuth.auth.signOut();
    }
    
    addUser(user, name: string) {
      let uid = user.uid;
      let ref = this.db.database.ref('/'); // the main endpoint of the db
      ref.child('users').child(uid).push({'name':name}); // if the child doesn't exist - he creates it
     }
     
    
      user: Observable<firebase.User>;
    constructor(private fireBaseAuth:AngularFireAuth,private db:AngularFireDatabase
    ) {
      this.user = fireBaseAuth.authState; }
}
