import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  addTodo(text:string,status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text, 'status':false});
     })
   }
 
    
     updateTodo(key:string, text:string, status:boolean){
        this.authService.user.subscribe(user =>{
         this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text,'status':status});
         })
       }
 
 

         updateStatus(key:string, text:string, status:boolean)
       {
         this.authService.user.subscribe(user =>{
          this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'status':status});
         })
         
       }
  
  deleteTodo(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').remove(key);
      }
    )
  }

   
  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }
 /*
  addTodo(todo: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').push({'text': todo,'done':'not done'});
      }
    )
  }
  
  updateDone(key:string, text:string, done:boolean) //checkbox
   {
      this.authService.user.subscribe(user =>{
     this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
      })
      
    }
      update(key, text) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').update(key,{'text':text});
      }
    )
  } */
}
